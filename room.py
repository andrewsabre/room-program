print"Please enter the dimensions for the floor in metres"
Length = float(input("Please enter the length of the floor: "))
Width = float(input("Please enter the width of the floor: "))
Area = (Length*Width)
#for rooms that are not a perfect cube
another = raw_input("Do you wish to add another section of floor? 'y/n' ")
if another == "y":
    Length2 = float(input("Please enter the length of the floor: "))
    Width2 = float(input("Please enter the width of the floor: "))
    Area2 = (Length2*Width2)
else:
    Area2 = 0



print"Please enter the dimensions for each wall individually in metres"
H1 = float(input("Please enter the height of the wall: "))
W1 = float(input("Please enter the width of the wall: "))
Warea = (H1*W1)

extra = raw_input("Do you wish to add another wall? 'y/n' ")
if extra == "y":
    H2 = float(input("Please enter the height of the wall: "))
    W2 = float(input("Please enter the width of the wall: "))
    Warea2 =(H2*W2)
    extra2 = raw_input("Do you wish to add another wall? 'y/n' ")
    if extra2 == "y":
            H3 = float(input("Please enter the height of the wall: "))
            W3 = float(input("Please enter the width of the wall: "))
            Warea3 = (H3*W3)
            extra3 = raw_input("Do you wish to add another wall? 'y/n' ")
            if extra3 == "y":
                    H4 = float(input("Please enter the height of the wall: "))
                    W4 = float(input("Please enter the width of the wall: "))
                    Warea4 = (H4*W4)
                    extra4 = raw_input("Do you wish to add another wall? 'y/n' ")
                    if extra4 == "y":
                            H5 = float(input("Please enter the height of the wall: "))
                            W5 = float(input("Please enter the width of the wall: "))
                            Warea5 = (H5*W5)
                            extra5 = raw_input("Do you wish to add another wall? 'y/n' ")
                            if extra5 == "y":
                                    H6 = float(input("Please enter the height of the wall: "))
                                    W6 = float(input("Please enter the width of the wall: "))
                                    Warea6 = (H6*W6)
                            else:
                                    Warea6 = 0
                    else:
                            Warea5 = 0
                            Warea6 = 0
            else:
                    Warea4 = 0
                    Warea5 = 0
                    Warea6 = 0
    else:
            Warea3 = 0
            Warea4 = 0
            Warea5 = 0
            Warea6 = 0
else:
        Warea2 = 0
        Warea3 = 0
        Warea4 = 0
        Warea5 = 0
        Warea6 = 0


#above allows up to six walls in the room, can be changed easily but for this example i'll keep it to a maximum of six
#indents in correlation to the if statements
Total1 = (Area+Area2)
Total2 = (Warea+Warea2+Warea3+Warea4+Warea5+Warea6)
Vol = (Total1*H1)
Paint = (Total2/10)
#above are the formulas used to calculate the output from the input
print "This is the area of the floor ",Total1, "metres squared"
print "This is the volume of the room", Vol, "metres squared"
print "This is the total area of the walls in the room", Total2, "metres squared"
print "The amount of litres you will need to paint all the walls in the room is", Paint,"this is based on a coverage of 10 metres squared per litre of paint"
